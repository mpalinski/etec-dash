# Implementation framework of ETEC's dashboard

## Getting started

### Prerequisites
Please include the following scripts in your project:

`<script src="https://cdn.amcharts.com/lib/5/index.js"></script>`

`<script src="https://cdn.amcharts.com/lib/5/xy.js"></script>`

`<script src="https://cdn.amcharts.com/lib/5/themes/Animated.js"></script>`

### Charts
There are two types of files in the `charts` folder:
- `*.js` chart's script ready to use in the final `<div>`, the div's id should use the same name as the script, e.g. `<div id="chart_1a">`
- `*.html` demo version - incorporating chart's script in the basic html structure. All charts can be accessed as `<iframe>`, e.g. https://mpalinski.gitlab.io/etec-dash/charts/chart_1a.html simply substitute the right chart number.


### Data
In order to reproduce chart's creation or prepare similar charts you can follow the steps described in the Jupyter Notebook (Basic Python knowledge is required) `./scripts/data_cleaning.ipynb`.
1. Download relevant TIMSS data (all paths are specified in the notebook)
2. Use function `clean_timss`
3. Create basic chart's elements in the format suitable for Amchart (visualization library), i.e. list of countries, scores for 2015 & 2019, scores differences (all steps are shown in the notebook)
4. Use these data as lists assigned to js variables (names, score_2015, score_2019, diff), e.g.:

var names = ['Singapore',
'Chinese Taipei',
'South Korea',
'Japan',
'Hong Kong',
'Russia',
'Ireland',
'Lithuania',
'Israel',
'Australia',
'Hungary',
'USA',
'England',
'Norway',
'Sweden',
'Italy',
'Turkey',
'New Zealand',
'Bahrain',
'UAE',
'Georgia',
'Malaysia',
'Iran',
'Qatar',
'Chile',
'Lebanon',
'Jordan',
'Egypt',
'Oman',
'Kuwait',
'Saudi Arabia',
'South Africa',
'Morocco'];

You can save these lists as files directly from the notebook.
