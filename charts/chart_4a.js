am5.ready(function() {

// Create root element
// https://www.amcharts.com/docs/v5/getting-started/#Root_element
var root = am5.Root.new("chart_4a");

// Set themes
// https://www.amcharts.com/docs/v5/concepts/themes/
root.setThemes([
  am5themes_Animated.new(root)
]);

// Create chart
// https://www.amcharts.com/docs/v5/charts/xy-chart/
var chart = root.container.children.push(
  am5xy.XYChart.new(root, {
    panX: false,
    panY: false,
    wheelX: "panX",
    wheelY: "zoomX",
    layout: root.verticalLayout,
    arrangeTooltips: false
  })
);

// Use only absolute numbers
chart.getNumberFormatter().set("numberFormat", "#.#s");

// Add legend
// https://www.amcharts.com/docs/v5/charts/xy-chart/legend-xy-series/
var legend = chart.children.push(
  am5.Legend.new(root, {
    centerX: am5.p50,
    x: am5.p50
  })
);

// Data
var data = [
  {country: 'Philippines', girls: -34.9 },
  {country: 'Saudi Arabia', girls: -26.5 },
  {country: 'South Africa ', girls: -20.3 },
  {country: 'Pakistan', girls: -18.9 },
  {country: 'Oman', girls: -14.0 },
  {country: 'Kuwait', girls: -6.5 },
  {country: 'Bahrain', girls: -4.8 },
  {country: 'Azerbaijan', girls: -3.7 },
  {country: 'Morocco', girls: -3.5 },
  {country: 'Armenia', girls: -2.4 },
  {country: 'Serbia', girls: -1.7 },
  {country: 'Qatar', girls: -1.1 },
  {country: 'Japan', girls: -0.8 },
  {country: 'Kazakhstan', girls: -0.4 },
  {country: 'North Macedonia', girls: -0.3 },
  {country: 'Bulgaria', boys: 2.2 },
  {country: 'Finland', boys: 2.6 },
  {country: 'Albania', boys: 2.7 },
  {country: 'Northern Ireland', boys: 3.5 },
  {country: 'Turkey ', boys: 3.5 },
  {country: 'Chinese Taipei', boys: 3.8 },
  {country: 'Norway ', boys: 4.4 },
  {country: 'Kosovo', boys: 4.5 },
  {country: 'Lithuania', boys: 4.6 },
  {country: 'Latvia', boys: 4.6 },
  {country: 'Montenegro', boys: 4.9 },
  {country: 'Korea', boys: 5.0 },
  {country: 'New Zealand', boys: 5.4 },
  {country: 'Hong Kong SAR', boys: 5.8 },
  {country: 'Ireland', boys: 6.8 },
  {country: 'Denmark', boys: 6.9 },
  {country: 'Sweden', boys: 7.1 },
  {country: 'England', boys: 7.3 },
  {country: 'Iran', boys: 7.4 },
  {country: 'Malta', boys: 7.5 },
  {country: 'Georgia', boys: 7.5 },
  {country: 'Austria', boys: 7.5 },
  {country: 'Singapore', boys: 7.9 },
  {country: 'Poland', boys: 8.1 },
  {country: 'United Arab Emirates', boys: 8.2 },
  {country: 'Russian Federation', boys: 8.2 },
  {country: 'Netherlands', boys: 8.5 },
  {country: 'Chile', boys: 8.5 },
  {country: 'Bosnia and Herzegovina', boys: 8.8 },
  {country: 'Australia', boys: 10.0 },
  {country: 'Germany', boys: 10.5 },
  {country: 'Belgium', boys: 10.8 },
  {country: 'Czech Republic', boys: 10.9 },
  {country: 'United States', boys: 11.1 },
  {country: 'Hungary', boys: 11.2 },
  {country: 'Croatia', boys: 11.8 },
  {country: 'Italy', boys: 12.3 },
  {country: 'Slovak Republic', boys: 12.5 },
  {country: 'France', boys: 13.6 },
  {country: 'Spain', boys: 14.6 },
  {country: 'Portugal', boys: 17.3 },
  {country: 'Cyprus', boys: 18.7 },
  {country: 'Canada', boys: 18.9 },
];

// Create axes
// https://www.amcharts.com/docs/v5/charts/xy-chart/axes/
var yAxis = chart.yAxes.push(
  am5xy.CategoryAxis.new(root, {
    categoryField: "country",
    renderer: am5xy.AxisRendererY.new(root, {
      minGridDistance: 15,
      inversed: true,
      cellStartLocation: 0.1,
      cellEndLocation: 0.9
    })
  })
);

yAxis.data.setAll(data);

var xAxis = chart.xAxes.push(
  am5xy.ValueAxis.new(root, {
    renderer: am5xy.AxisRendererX.new(root, {})
  })
);

// Add series
// https://www.amcharts.com/docs/v5/charts/xy-chart/series/
function createSeries(field, labelCenterX, pointerOrientation, rangeValue) {
  var series = chart.series.push(
    am5xy.ColumnSeries.new(root, {
      xAxis: xAxis,
      yAxis: yAxis,
      valueXField: field,
      categoryYField: "country",
      sequencedInterpolation: true,
      clustered: false,
      tooltip: am5.Tooltip.new(root, {
        pointerOrientation: pointerOrientation,
        labelText: "{categoryY}: {valueX}"
      })
    })
  );

  series.columns.template.setAll({
    height: am5.p100
  });

  // series.bullets.push(function() {
  //   return am5.Bullet.new(root, {
  //     locationX: 1,
  //     locationY: 0.5,
  //     sprite: am5.Label.new(root, {
  //       centerY: am5.p50,
  //       text: "{valueX}",
  //       populateText: true,
  //       centerX: labelCenterX
  //     })
  //   });
  // });

  series.data.setAll(data);
  series.appear();

  // var rangeDataItem = xAxis.makeDataItem({
  //   value: rangeValue
  // });
  // xAxis.createAxisRange(rangeDataItem);
  // rangeDataItem.get("grid").setAll({
  //   strokeOpacity: 1,
  //   stroke: series.get("stroke")
  // });
  //
  // var label = rangeDataItem.get("label");
  // label.setAll({
  //   text: field.toUpperCase(),
  //   fontSize: "2.1em",
  //   fill: series.get("stroke"),
  //   paddingTop: 30,
  //   isMeasured: false,
  //   centerX: labelCenterX
  // });
  // label.adapters.add("dy", function() {
  //   return -chart.plotContainer.height();
  // });

  return series;
}

createSeries("girls", 0, "left", -40);
createSeries("boys", am5.p100, "right", 20);

// Add cursor
// https://www.amcharts.com/docs/v5/charts/xy-chart/cursor/
var cursor = chart.set("cursor", am5xy.XYCursor.new(root, {
  behavior: "zoomY"
}));
cursor.lineY.set("forceHidden", true);
cursor.lineX.set("forceHidden", true);
// Make stuff animate on load
// https://www.amcharts.com/docs/v5/concepts/animations/
chart.appear(1000, 100);

}); // end am5.ready()
