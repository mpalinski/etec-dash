am5.ready(function() {

  // Create root element
  // https://www.amcharts.com/docs/v5/getting-started/#Root_element
var root = am5.Root.new("chart_1b");

// Set themes
// https://www.amcharts.com/docs/v5/concepts/themes/
root.setThemes([
  am5themes_Animated.new(root)
]);

// Create chart
// https://www.amcharts.com/docs/v5/charts/xy-chart/
var chart = root.container.children.push(am5xy.XYChart.new(root, {
  panX: true,
  panY: true,
  wheelX: "panY",
  wheelY: "zoomY"
}));

chart.get("colors").set("step", 3);


// Add cursor
// https://www.amcharts.com/docs/v5/charts/xy-chart/cursor/
var cursor = chart.set("cursor", am5xy.XYCursor.new(root, {}));
cursor.lineY.set("visible", true);


// Create axes
// https://www.amcharts.com/docs/v5/charts/xy-chart/axes/
var yRenderer = am5xy.AxisRendererY.new(root, {
  minGridDistance: 15,
  inversed: true
});

var yAxis = chart.yAxes.push(am5xy.CategoryAxis.new(root, {
  maxDeviation: 0.3,
  categoryField: "category",
  renderer: yRenderer,
  tooltip: am5.Tooltip.new(root, {})
}));

var xAxis = chart.xAxes.push(am5xy.ValueAxis.new(root, {
  maxDeviation: 0.3,
  renderer: am5xy.AxisRendererX.new(root, {})
}));


// Create series
// https://www.amcharts.com/docs/v5/charts/xy-chart/series/
var series = chart.series.push(am5xy.ColumnSeries.new(root, {
  xAxis: xAxis,
  yAxis: yAxis,
  baseAxis: yAxis,
  valueXField: "close",
  openValueXField: "open",
  categoryYField: "category",
  tooltip: am5.Tooltip.new(root, {
    labelText: "Difference between average scores: {diff}"
  })
}));

series.columns.template.setAll({
  height: 0.5
});

series.bullets.push(function() {
  return am5.Bullet.new(root, {
    locationX: 0,
    sprite: am5.Circle.new(root, {
      radius: 5,
      fill: series.get("fill")
    })
  })
})

var nextColor = chart.get("colors").getIndex(1);

// Create range axis data item
let rangeDataItem = yAxis.makeDataItem({
  category: "Saudi Arabia"
});

// Create a range
let range = yAxis.createAxisRange(rangeDataItem);

rangeDataItem.get("label").setAll({
  fill: am5.color(0xffffff),
  text: "Saudi Arabia",
  background: am5.RoundedRectangle.new(root, {
    fill: nextColor
  })
});

rangeDataItem.get("grid").set("visible", false);

series.bullets.push(function() {
  return am5.Bullet.new(root, {
    locationX: 1,
    sprite: am5.Circle.new(root, {
      radius: 5,
      fill: nextColor
    })
  })
})

var data = []
var score_2015 = [621.0,
599.0,
606.0,
586.0,
594.0,
538.0,
523.0,
511.0,
511.0,
505.0,
514.0,
518.0,
518.0,
512.0,
501.0,
494.0,
458.0,
493.0,
454.0,
465.0,
453.0,
465.0,
436.0,
437.0,
427.0,
442.0,
386.0,
392.0,
403.0,
392.0,
368.0,
372.0,
384.0
];
var score_2019 = [616.0,
612.0,
607.0,
594.0,
578.0,
543.0,
524.0,
520.0,
519.0,
517.0,
517.0,
515.0,
515.0,
503.0,
503.0,
497.0,
496.0,
482.0,
481.0,
473.0,
461.0,
461.0,
446.0,
443.0,
441.0,
429.0,
420.0,
413.0,
411.0,
403.0,
394.0,
389.0,
388.0
];

var diff = [
  -5.0,
13.0,
1.0,
8.0,
-16.0,
5.0,
0.0,
9.0,
8.0,
12.0,
2.0,
-3.0,
-3.0,
-9.0,
2.0,
3.0,
38.0,
-11.0,
27.0,
9.0,
8.0,
-5.0,
10.0,
6.0,
13.0,
-13.0,
35.0,
21.0,
8.0,
10.0,
26.0,
17.0,
4.0,
]
var names = ['Singapore',
'Chinese Taipei',
'South Korea',
'Japan',
'Hong Kong',
'Russia',
'Ireland',
'Lithuania',
'Israel',
'Australia',
'Hungary',
'USA',
'England',
'Norway',
'Sweden',
'Italy',
'Turkey',
'New Zealand',
'Bahrain',
'UAE',
'Georgia',
'Malaysia',
'Iran',
'Qatar',
'Chile',
'Lebanon',
'Jordan',
'Egypt',
'Oman',
'Kuwait',
'Saudi Arabia',
'South Africa',
'Morocco'];

for (var i = 0; i < names.length; i++) {


  data.push({ category: names[i], open: score_2015[i], close: score_2019[i], diff: diff[i] });
}

yAxis.data.setAll(data);
series.data.setAll(data);


// Make stuff animate on load
// https://www.amcharts.com/docs/v5/concepts/animations/
series.appear(1000);
chart.appear(1000, 100);

}); // end am5.ready()
