am5.ready(function() {

  // Create root element
  // https://www.amcharts.com/docs/v5/getting-started/#Root_element
var root = am5.Root.new("chart_2b");

// Set themes
// https://www.amcharts.com/docs/v5/concepts/themes/
root.setThemes([
  am5themes_Animated.new(root)
]);

// Create chart
// https://www.amcharts.com/docs/v5/charts/xy-chart/
var chart = root.container.children.push(am5xy.XYChart.new(root, {
  panX: true,
  panY: true,
  wheelX: "panY",
  wheelY: "zoomY"
}));

chart.get("colors").set("step", 3);


// Add cursor
// https://www.amcharts.com/docs/v5/charts/xy-chart/cursor/
var cursor = chart.set("cursor", am5xy.XYCursor.new(root, {}));
cursor.lineY.set("visible", true);


// Create axes
// https://www.amcharts.com/docs/v5/charts/xy-chart/axes/
var yRenderer = am5xy.AxisRendererY.new(root, {
  minGridDistance: 15,
  inversed: true
});

var yAxis = chart.yAxes.push(am5xy.CategoryAxis.new(root, {
  maxDeviation: 0.3,
  categoryField: "category",
  renderer: yRenderer,
  tooltip: am5.Tooltip.new(root, {})
}));

var xAxis = chart.xAxes.push(am5xy.ValueAxis.new(root, {
  maxDeviation: 0.3,
  renderer: am5xy.AxisRendererX.new(root, {})
}));


// Create series
// https://www.amcharts.com/docs/v5/charts/xy-chart/series/
var series = chart.series.push(am5xy.ColumnSeries.new(root, {
  xAxis: xAxis,
  yAxis: yAxis,
  baseAxis: yAxis,
  valueXField: "close",
  openValueXField: "open",
  categoryYField: "category",
  tooltip: am5.Tooltip.new(root, {
    labelText: "Difference between average scores: {diff}"
  })
}));

series.columns.template.setAll({
  height: 0.5
});

series.bullets.push(function() {
  return am5.Bullet.new(root, {
    locationX: 0,
    sprite: am5.Circle.new(root, {
      radius: 5,
      fill: series.get("fill")
    })
  })
})

var nextColor = chart.get("colors").getIndex(1);

// Create range axis data item
let rangeDataItem = yAxis.makeDataItem({
  category: "Saudi Arabia"
});

// Create a range
let range = yAxis.createAxisRange(rangeDataItem);

rangeDataItem.get("label").setAll({
  fill: am5.color(0xffffff),
  text: "Saudi Arabia",
  // location: 1,
  background: am5.RoundedRectangle.new(root, {
    fill: nextColor
  })
});

rangeDataItem.get("grid").set("visible", false);

series.bullets.push(function() {
  return am5.Bullet.new(root, {
    locationX: 1,
    sprite: am5.Circle.new(root, {
      radius: 5,
      fill: nextColor
    })
  })
})

var data = []
var score_2015 = [618.0,
615.0,
608.0,
597.0,
593.0,
564.0,
570.0,
546.0,
547.0,
549.0,
535.0,
530.0,
539.0,
528.0,
546.0,
523.0,
535.0,
541.0,
539.0,
529.0,
519.0,
522.0,
535.0,
517.0,
524.0,
507.0,
511.0,
498.0,
502.0,
518.0,
505.0,
481.0,
491.0,
488.0,
463.0,
452.0,
451.0,
439.0,
431.0,
459.0,
425.0,
383.0,
377.0,
353.0,
376.0,
];
var score_2019 = [625.0,
602.0,
600.0,
599.0,
593.0,
567.0,
566.0,
556.0,
548.0,
543.0,
542.0,
538.0,
535.0,
533.0,
532.0,
532.0,
532.0,
525.0,
525.0,
523.0,
521.0,
521.0,
520.0,
516.0,
515.0,
515.0,
512.0,
510.0,
509.0,
508.0,
502.0,
498.0,
487.0,
485.0,
482.0,
481.0,
480.0,
449.0,
443.0,
441.0,
431.0,
398.0,
383.0,
383.0,
374.0,
];

var diff = [
  8.0,
  -13.0,
  -8.0,
  3.0,
  0.0,
  3.0,
  -4.0,
  10.0,
  1.0,
  -6.0,
  7.0,
  8.0,
  -4.0,
  5.0,
  -13.0,
  9.0,
  -3.0,
  -16.0,
  -14.0,
  -6.0,
  3.0,
  -1.0,
  -15.0,
  -1.0,
  -9.0,
  8.0,
  1.0,
  12.0,
  7.0,
  -10.0,
  -3.0,
  17.0,
  -3.0,
  -3.0,
  19.0,
  30.0,
  29.0,
  10.0,
  12.0,
  -18.0,
  5.0,
  15.0,
  6.0,
  30.0,
  -2.0,
]
var names = ['Singapore',
'Hong Kong',
'South Korea',
'Chinese Taipei',
'Japan',
'Russia',
'Northern Ireland',
'England',
'Ireland',
'Norway',
'Lithuania',
'Netherlands',
'USA',
'Czech Republic',
'Belgium',
'Cyprus',
'Finland',
'Portugal',
'Denmark',
'Hungary',
'Sweden',
'Germany',
'Poland',
'Australia',
'Bulgaria',
'Italy',
'Canada',
'Slovak Republic',
'Croatia',
'Serbia',
'Spain',
'Armenia',
'New Zealand',
'France',
'Georgia',
'UAE',
'Bahrain',
'Qatar',
'Iran',
'Chile',
'Oman',
'Saudi Arabia',
'Morocco',
'Kuwait',
'South Africa',];


for (var i = 0; i < names.length; i++) {


  data.push({ category: names[i], open: score_2015[i], close: score_2019[i], diff: diff[i] });
}

yAxis.data.setAll(data);
series.data.setAll(data);


// Make stuff animate on load
// https://www.amcharts.com/docs/v5/concepts/animations/
series.appear(1000);
chart.appear(1000, 100);

}); // end am5.ready()
