am4core.ready(function() {

                  // Themes begin
                  am4core.useTheme(am4themes_animated);
                  // Themes end

                   // Create map instance
                  var chart = am4core.create("chart_10", am4maps.MapChart);
                  chart.chartContainer.wheelable = false;

                  // Set map definition
                  try {
                      chart.geodata = am4geodata_worldHigh;
                  }
                  catch (e) {
                      chart.raiseCriticalError(new Error("Map geodata could not be loaded. Please download the latest <a href=\"https://www.amcharts.com/download/download-v4/\">amcharts geodata</a> and extract its contents into the same directory as your amCharts files."));
                  }

                  // Set projection
                  chart.projection = new am4maps.projections.Mercator();

                  // Create map polygon series
                  // var polygonSeries = chart.series.push(new am4maps.MapPolygonSeries());
                  // map polygon series (countries)
                  var polygonSeries = chart.series.push(new am4maps.MapPolygonSeries());
                  polygonSeries.useGeodata = true;
                  // specify which countries to include

                  polygonSeries.exclude = ["AQ"];

                  // country area look and behavior
                  var polygonTemplate = polygonSeries.mapPolygons.template;
                  polygonTemplate.strokeOpacity = 1;
                  polygonTemplate.stroke = am4core.color("#ffffff");
                  polygonTemplate.fillOpacity = 0.9;
                  polygonTemplate.tooltipText = "{name}";


                  //Set min/max fill color for each area
                  polygonSeries.heatRules.push({
                    property: "fill",
                    target: polygonSeries.mapPolygons.template,
                    min: chart.colors.getIndex(1).brighten(1),
                    max: chart.colors.getIndex(1).brighten(-.7)
                  });

                  // Make map load polygon data (state shapes and names) from GeoJSON
                  polygonSeries.useGeodata = true;

                  // Set heatmap values for each state
                  polygonSeries.data = [
                    {cnt: "Albania",
  id: "AL",
  value: 489
  },
  {cnt: "Armenia",
  id: "AM",
  value: 466
  },
  {cnt: "Australia",
  id: "AU",
  value: 533
  },
  {cnt: "Austria",
  id: "AT",
  value: 522
  },
  {cnt: "Azerbaijan",
  id: "AZ",
  value: 427
  },
  {cnt: "Bahrain",
  id: "BH",
  value: 493
  },
  {cnt: "Belgium ",
  id: "BE",
  value: 501
  },
  {cnt: "Bosnia and Herzegovina",
  id: "BA",
  value: 459
  },
  {cnt: "Bulgaria",
  id: "BG",
  value: 521
  },
  {cnt: "Canada",
  id: "CA",
  value: 523
  },
  {cnt: "Chile",
  id: "CL",
  value: 469
  },
  {cnt: "Chinese Taipei",
  id: "TW",
  value: 558
  },
  {cnt: "Croatia",
  id: "HR",
  value: 524
  },
  {cnt: "Cyprus",
  id: "CY",
  value: 511
  },
  {cnt: "Czech Republic",
  id: "CZ",
  value: 534
  },
  {cnt: "Denmark",
  id: "DK",
  value: 522
  },
  {cnt: "England",
  id: "GB",
  value: 537
  },
  {cnt: "Finland",
  id: "FI",
  value: 555
  },
  {cnt: "France",
  id: "FR",
  value: 488
  },
  {cnt: "Georgia",
  id: "GE",
  value: 454
  },
  {cnt: "Germany",
  id: "DE",
  value: 518
  },
  {cnt: "Hong Kong SAR",
  id: "HK",
  value: 531
  },
  {cnt: "Hungary",
  id: "HU",
  value: 529
  },
  {cnt: "Iran",
  id: "IR",
  value: 441
  },
  {cnt: "Ireland",
  id: "IE",
  value: 528
  },
  {cnt: "Italy",
  id: "IT",
  value: 510
  },
  {cnt: "Japan",
  id: "JP",
  value: 562
  },
  {cnt: "Kazakhstan",
  id: "KZ",
  value: 494
  },
  {cnt: "Kosovo",
  id: "XK",
  value: 413
  },
  {cnt: "Kuwait",
  id: "KW",
  value: 392
  },
  {cnt: "Latvia",
  id: "LV",
  value: 542
  },
  {cnt: "Lithuania",
  id: "LT",
  value: 538
  },
  {cnt: "Malta",
  id: "MT",
  value: 496
  },
  {cnt: "Montenegro",
  id: "ME",
  value: 453
  },
  {cnt: "Morocco",
  id: "MA",
  value: 374
  },
  {cnt: "Netherlands",
  id: "NL",
  value: 518
  },
  {cnt: "New Zealand",
  id: "NZ",
  value: 503
  },
  {cnt: "North Macedonia",
  id: "MK",
  value: 426
  },
  {cnt: "Northern Ireland",
  id: "XI",
  value: 518
  },
  {cnt: "Norway ",
  id: "NO",
  value: 539
  },
  {cnt: "Oman",
  id: "OM",
  value: 435
  },
  {cnt: "Pakistan",
  id: "PK",
  value: 290
  },
  {cnt: "Philippines",
  id: "PH",
  value: 249
  },
  {cnt: "Poland",
  id: "PL",
  value: 531
  },
  {cnt: "Portugal",
  id: "PT",
  value: 504
  },
  {cnt: "Qatar",
  id: "QA",
  value: 449
  },
  {cnt: "Russian Federation",
  id: "RU",
  value: 567
  },
  {cnt: "Saudi Arabia",
  id: "SA",
  value: 402
  },
  {cnt: "Serbia",
  id: "RS",
  value: 517
  },
  {cnt: "Singapore",
  id: "SG",
  value: 595
  },
  {cnt: "Slovak Republic",
  id: "SK",
  value: 521
  },
  {cnt: "South Africa ",
  id: "ZA",
  value: 324
  },
  {cnt: "South Korea",
  id: "KR",
  value: 588
  },
  {cnt: "Spain",
  id: "ES",
  value: 511
  },
  {cnt: "Sweden",
  id: "SE",
  value: 537
  },
  {cnt: "Turkey ",
  id: "TR",
  value: 526
  },
  {cnt: "United Arab Emirates",
  id: "AE",
  value: 473
  },
  {cnt: "United States",
  id: "US",
  value: 539
  },

                  ];

                  // Set up heat legend
                  let heatLegend = chart.createChild(am4maps.HeatLegend);
                  heatLegend.series = polygonSeries;
                  heatLegend.align = "right";
                  heatLegend.valign = "bottom";
                  heatLegend.width = am4core.percent(20);
                  heatLegend.valueAxis.renderer.labels.template.fontSize = 14;
                  heatLegend.marginRight = am4core.percent(35);
                  heatLegend.minValue = 200;
                  heatLegend.maxValue = 700;

                  // Set up custom heat map legend labels using axis ranges
                  var minRange = heatLegend.valueAxis.axisRanges.create();
                  minRange.value = heatLegend.minValue;
                  minRange.label.text = "200";
                  var maxRange = heatLegend.valueAxis.axisRanges.create();
                  maxRange.value = heatLegend.maxValue;
                  maxRange.label.text = "700";

                  // Blank out internal heat legend value axis labels
                  heatLegend.valueAxis.renderer.labels.template.adapter.add("text", function(labelText) {
                    return "";
                  });

                  // Configure series tooltip
                  var polygonTemplate = polygonSeries.mapPolygons.template;
                  polygonTemplate.tooltipText = "[bold]{cnt}[\] \n Science result: {value}";
                  polygonTemplate.nonScalingStroke = true;
                  polygonTemplate.strokeWidth = 0.5;

                  // Create hover state and set alternative fill color
                  var hs = polygonTemplate.states.create("hover");
                  hs.properties.fill = am4core.color("#3c5bdc");

                  }); // end am4core.ready()
