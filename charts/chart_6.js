am4core.ready(function() {

    // Themes begin
    am4core.useTheme(am4themes_animated);
    // Themes end

    var chart = am4core.create("chart_6", am4charts.XYChart);

    var data = [
    {category: "Philippines",
    cnt: "Filipiny",
    value: 297
    },
    {category: "Pakistan",
    cnt: "Pakistan",
    value: 328
    },
    {category: "South-africa",
    cnt: "RPA",
    value: 374
    },
    {category: "Morocco",
    cnt: "Maroko",
    value: 383
    },
    {category: "Kuwait",
    cnt: "Kuwejt",
    value: 383
    },
    {category: "Saudi Arabia",
    cnt: "Arabia Saudyjska",
    value: 398
    },
    {category: "Oman",
    cnt: "Oman",
    value: 431
    },
    {category: "Chile",
    cnt: "Chile",
    value: 441
    },
    {category: "Iran",
    cnt: "Iran",
    value: 443
    },
    {category: "Kosovo",
    cnt: "Kosowo",
    value: 444
    },
    {category: "Qatar",
    cnt: "Katar",
    value: 449
    },
    {category: "Bosnia and Herzegovina",
    cnt: "Bośnia i Hercegowina",
    value: 452
    },
    {category: "Montenegro",
    cnt: "Czarnogóra",
    value: 453
    },
    {category: "Republic-of-Macedonia",
    cnt: "Macedonia Północna",
    value: 472
    },
    {category: "Bahrain",
    cnt: "Bahrajn",
    value: 480
    },
    {category: "United Arab Emirates",
    cnt: "Zjednoczone Emiraty Arabskie",
    value: 481
    },
    {category: "Georgia",
    cnt: "Gruzja",
    value: 482
    },
    {category: "France",
    cnt: "Francja",
    value: 485
    },
    {category: "New Zealand",
    cnt: "Nowa Zelandia",
    value: 487
    },
    {category: "Albania",
    cnt: "Albania",
    value: 494
    },
    {category: "Armenia",
    cnt: "Armenia",
    value: 498
    },
    {category: "Spain",
    cnt: "Hiszpania",
    value: 502
    },
    {category: "Serbia",
    cnt: "Serbia",
    value: 508
    },
    {category: "Croatia",
    cnt: "Chorwacja",
    value: 509
    },
    {category: "Malta",
    cnt: "Malta",
    value: 509
    },
    {category: "Slovakia",
    cnt: "Słowacja",
    value: 510
    },
    {category: "Kazakhstan",
    cnt: "Kazachstan",
    value: 512
    },
    {category: "Canada",
    cnt: "Kanada",
    value: 512
    },
    {category: "Azerbaijan",
    cnt: "Azerbejdżan",
    value: 515
    },
    {category: "Bulgaria",
    cnt: "Bułgaria",
    value: 515
    },
    {category: "Italy",
    cnt: "Włochy",
    value: 515
    },
    {category: "Australia",
    cnt: "Australia",
    value: 516
    },
    {category: "Republic of Poland",
    cnt: "Polska",
    value: 520
    },
    {category: "Sweden",
    cnt: "Szwecja",
    value: 521
    },
    {category: "Germany",
    cnt: "Niemcy",
    value: 521
    },
    {category: "Hungary",
    cnt: "Węgry",
    value: 523
    },
    {category: "Turkey",
    cnt: "Turcja",
    value: 523
    },
    {category: "Portugal",
    cnt: "Portugalia",
    value: 525
    },
    {category: "Denmark",
    cnt: "Dania",
    value: 525
    },
    {category: "Belgium",
    cnt: "Belgia",
    value: 532
    },
    {category: "Cyprus",
    cnt: "Cypr",
    value: 532
    },
    {category: "Finland",
    cnt: "Finlandia",
    value: 532
    },
    {category: "Czech Republic",
    cnt: "Czechy",
    value: 533
    },
    {category: "United States",
    cnt: "USA",
    value: 535
    },
    {category: "Netherlands",
    cnt: "Holandia",
    value: 538
    },
    {category: "Austria",
    cnt: "Austria",
    value: 539
    },
    {category: "Lithuania",
    cnt: "Litwa",
    value: 542
    },
    {category: "Norway",
    cnt: "Norwegia",
    value: 543
    },
    {category: "Latvia",
    cnt: "Łotwa",
    value: 546
    },
    {category: "Ireland",
    cnt: "Irlandia",
    value: 548
    },
    {category: "England",
    cnt: "Anglia",
    value: 556
    },
    {category: "United-kingdom",
    cnt: "Irlandia Północna",
    value: 566
    },
    {category: "Russia",
    cnt: "Rosja",
    value: 567
    },
    {category: "Japan",
    cnt: "Japonia",
    value: 593
    },
    {category: "Taiwan",
    cnt: "Tajwan",
    value: 599
    },
    {category: "South Korea",
    cnt: "Korea Południowa",
    value: 600
    },
    {category: "Hong-kong",
    cnt: "Hongkong",
    value: 602
    },
    {category: "Singapore",
    cnt: "Singapur",
    value: 625
    },
    ];


    chart.data = data;
    var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
    categoryAxis.renderer.grid.template.location = 0;
    categoryAxis.dataFields.category = "category";
    categoryAxis.renderer.minGridDistance = 15;
    categoryAxis.renderer.grid.template.location = 0.5;
    categoryAxis.renderer.grid.template.strokeDasharray = "1,3";
    categoryAxis.renderer.labels.template.rotation = -90;
    categoryAxis.renderer.labels.template.horizontalCenter = "left";
    categoryAxis.renderer.labels.template.location = 0.5;
    categoryAxis.tooltip.disabled = true;


    categoryAxis.renderer.labels.template.adapter.add("dx", function(dx, target) {
        return -target.maxRight / 2;
    })

    categoryAxis.renderer.labels.template.disabled = true;
    var image = new am4core.Image();
    image.horizontalCenter = "middle";
    image.width = 20;
    image.height = 20;
    image.verticalCenter = "middle";
    image.adapter.add("href", (href, target)=>{
      let category = target.dataItem.category;
      if(category){
        return "https://www.amcharts.com/wp-content/uploads/flags/" + category.split(" ").join("-").toLowerCase() + ".svg";
      }
      return href;
    })
    categoryAxis.dataItems.template.bullet = image;

    var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
    valueAxis.tooltip.disabled = true;
    valueAxis.renderer.ticks.template.disabled = true;
    valueAxis.renderer.axisFills.template.disabled = true;

    var series = chart.series.push(new am4charts.ColumnSeries());
    series.dataFields.categoryX = "category";
    series.dataFields.valueY = "value";
    series.tooltipText = "{category} {valueY.value}";
    series.sequencedInterpolation = true;
    series.fillOpacity = 0;
    series.strokeOpacity = 1;
    series.strokeDashArray = "1,3";
    series.columns.template.width = 0.01;
    series.tooltip.pointerOrientation = "horizontal";

    var bullet = series.bullets.create(am4charts.CircleBullet);

    chart.cursor = new am4charts.XYCursor();

    chart.scrollbarX = new am4core.Scrollbar();
    chart.scrollbarY = new am4core.Scrollbar();


    chart.events.on("ready", function () {
      categoryAxis.zoomToCategories(
      "Philippines","Singapur")
    })
    var range = categoryAxis.axisRanges.create();
    range.category = "Saudi Arabia";
    range.axisFill.fill = chart.colors.getIndex(1);
    range.axisFill.fillOpacity = 0.2;
    range.label.text = "";
    }); // end am4core.ready()
