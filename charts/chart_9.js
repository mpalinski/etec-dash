am4core.ready(function() {

  // Themes begin
  am4core.useTheme(am4themes_animated);
  // Themes end

  var chart = am4core.create("chart_9", am4charts.XYChart);

  var data = [
    {category: "Philippines",
    cnt: "Filipiny",
    value: 249
    },
    {category: "Pakistan",
    cnt: "Pakistan",
    value: 290
    },
    {category: "South-Africa",
    cnt: "RPA",
    value: 324
    },
    {category: "Morocco",
    cnt: "Maroko",
    value: 374
    },
    {category: "Kuwait",
    cnt: "Kuwejt",
    value: 392
    },
    {category: "Saudi Arabia",
    cnt: "Arabia Saudyjska",
    value: 402
    },
    {category: "Kosovo",
    cnt: "Kosowo",
    value: 413
    },
    {category: "Republic-of-Macedonia",
    cnt: "Macedonia Północna",
    value: 426
    },
    {category: "Azerbaijan",
    cnt: "Azerbejdżan",
    value: 427
    },
    {category: "Oman",
    cnt: "Oman",
    value: 435
    },
    {category: "Iran",
    cnt: "Iran",
    value: 441
    },
    {category: "Qatar",
    cnt: "Katar",
    value: 449
    },
    {category: "Montenegro",
    cnt: "Czarnogóra",
    value: 453
    },
    {category: "Georgia",
    cnt: "Gruzja",
    value: 454
    },
    {category: "Bosnia and Herzegovina",
    cnt: "Bośnia i Hercegowina",
    value: 459
    },
    {category: "Armenia",
    cnt: "Armenia",
    value: 466
    },
    {category: "Chile",
    cnt: "Chile",
    value: 469
    },
    {category: "United Arab Emirates",
    cnt: "Zjednoczone Emiraty Arabskie",
    value: 473
    },
    {category: "France",
    cnt: "Francja",
    value: 488
    },
    {category: "Albania",
    cnt: "Albania",
    value: 489
    },
    {category: "Bahrain",
    cnt: "Bahrajn",
    value: 493
    },
    {category: "Kazakhstan",
    cnt: "Kazachstan",
    value: 494
    },
    {category: "Malta",
    cnt: "Malta",
    value: 496
    },
    {category: "Belgium",
    cnt: "Belgia",
    value: 501
    },
    {category: "New Zealand",
    cnt: "Nowa Zelandia",
    value: 503
    },
    {category: "Portugal",
    cnt: "Portugalia",
    value: 504
    },
    {category: "Italy",
    cnt: "Włochy",
    value: 510
    },
    {category: "Spain",
    cnt: "Hiszpania",
    value: 511
    },
    {category: "Cyprus",
    cnt: "Cypr",
    value: 511
    },
    {category: "Serbia",
    cnt: "Serbia",
    value: 517
    },
    {category: "Germany",
    cnt: "Niemcy",
    value: 518
    },
    {category: "Netherlands",
    cnt: "Holandia",
    value: 518
    },
    {category: "United-Kingdom",
    cnt: "Irlandia Północna",
    value: 518
    },
    {category: "Bulgaria",
    cnt: "Bułgaria",
    value: 521
    },
    {category: "Slovakia",
    cnt: "Słowacja",
    value: 521
    },
    {category: "Denmark",
    cnt: "Dania",
    value: 522
    },
    {category: "Austria",
    cnt: "Austria",
    value: 522
    },
    {category: "Canada",
    cnt: "Kanada",
    value: 523
    },
    {category: "Croatia",
    cnt: "Chorwacja",
    value: 524
    },
    {category: "Turkey",
    cnt: "Turcja",
    value: 526
    },
    {category: "Ireland",
    cnt: "Irlandia",
    value: 528
    },
    {category: "Hungary",
    cnt: "Węgry",
    value: 529
    },
    {category: "Hong-Kong",
    cnt: "Hongkong",
    value: 531
    },
    {category: "Republic of Poland",
    cnt: "Polska",
    value: 531
    },
    {category: "Australia",
    cnt: "Australia",
    value: 533
    },
    {category: "Czech Republic",
    cnt: "Czechy",
    value: 534
    },
    {category: "England",
    cnt: "Anglia",
    value: 537
    },
    {category: "Sweden",
    cnt: "Szwecja",
    value: 537
    },
    {category: "Lithuania",
    cnt: "Litwa",
    value: 538
    },
    {category: "United States",
    cnt: "USA",
    value: 539
    },
    {category: "Norway",
    cnt: "Norwegia",
    value: 539
    },
    {category: "Latvia",
    cnt: "Łotwa",
    value: 542
    },
    {category: "Finland",
    cnt: "Finlandia",
    value: 555
    },
    {category: "Taiwan",
    cnt: "Tajwan",
    value: 558
    },
    {category: "Japan",
    cnt: "Japonia",
    value: 562
    },
    {category: "Russia",
    cnt: "Rosja",
    value: 567
    },
    {category: "South Korea",
    cnt: "Korea Południowa",
    value: 588
    },
    {category: "Singapore",
    cnt: "Singapur",
    value: 595
    },
  ];


  chart.data = data;
  var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
  categoryAxis.renderer.grid.template.location = 0;
  categoryAxis.dataFields.category = "category";
  categoryAxis.renderer.minGridDistance = 15;
  categoryAxis.renderer.grid.template.location = 0.5;
  categoryAxis.renderer.grid.template.strokeDasharray = "1,3";
  categoryAxis.renderer.labels.template.rotation = -90;
  categoryAxis.renderer.labels.template.horizontalCenter = "left";
  categoryAxis.renderer.labels.template.location = 0.5;
  categoryAxis.tooltip.disabled = true;


  categoryAxis.renderer.labels.template.adapter.add("dx", function(dx, target) {
      return -target.maxRight / 2;
  })

  categoryAxis.renderer.labels.template.disabled = true;
  var image = new am4core.Image();
  image.horizontalCenter = "middle";
  image.width = 20;
  image.height = 20;
  image.verticalCenter = "middle";
  image.adapter.add("href", (href, target)=>{
    let category = target.dataItem.category;
    if(category){
      return "https://www.amcharts.com/wp-content/uploads/flags/" + category.split(" ").join("-").toLowerCase() + ".svg";
    }
    return href;
  })
  categoryAxis.dataItems.template.bullet = image;

  var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
  valueAxis.tooltip.disabled = true;
  valueAxis.renderer.ticks.template.disabled = true;
  valueAxis.renderer.axisFills.template.disabled = true;

  var series = chart.series.push(new am4charts.ColumnSeries());
  series.dataFields.categoryX = "category";
  series.dataFields.valueY = "value";
  series.tooltipText = "{category} {valueY.value}";
  series.sequencedInterpolation = true;
  series.fillOpacity = 0;
  series.strokeOpacity = 1;
  series.strokeDashArray = "1,3";
  series.columns.template.width = 0.01;
  series.tooltip.pointerOrientation = "horizontal";

  var bullet = series.bullets.create(am4charts.CircleBullet);

  chart.cursor = new am4charts.XYCursor();

  chart.scrollbarX = new am4core.Scrollbar();
  chart.scrollbarY = new am4core.Scrollbar();


  chart.events.on("ready", function () {
    categoryAxis.zoomToCategories(
    "Philippines","Singapore")
  })

  var range = categoryAxis.axisRanges.create();
  range.category = "Saudi Arabia";
  range.axisFill.fill = chart.colors.getIndex(1);
  range.axisFill.fillOpacity = 0.2;
  range.label.text = "";
  }); // end am4core.ready()
