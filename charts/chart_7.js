am4core.ready(function() {

                    // Themes begin
                    am4core.useTheme(am4themes_animated);
                    // Themes end

                     // Create map instance
                    var chart = am4core.create("chart_7", am4maps.MapChart);
                    // chart.maxZoomLevel = 1;
                    chart.chartContainer.wheelable = false;
                    // Set map definition
                    try {
                        chart.geodata = am4geodata_worldHigh;
                    }
                    catch (e) {
                        chart.raiseCriticalError(new Error("Map geodata could not be loaded. Please download the latest <a href=\"https://www.amcharts.com/download/download-v4/\">amcharts geodata</a> and extract its contents into the same directory as your amCharts files."));
                    }

                    // Set projection
                    chart.projection = new am4maps.projections.Mercator();

                    // Create map polygon series
                    // var polygonSeries = chart.series.push(new am4maps.MapPolygonSeries());
                    // map polygon series (countries)
                    var polygonSeries = chart.series.push(new am4maps.MapPolygonSeries());
                    polygonSeries.useGeodata = true;
                    // specify which countries to include

                    polygonSeries.exclude = ["AQ"];

                    // country area look and behavior
                    var polygonTemplate = polygonSeries.mapPolygons.template;
                    polygonTemplate.strokeOpacity = 1;
                    polygonTemplate.stroke = am4core.color("#ffffff");
                    polygonTemplate.fillOpacity = 0.9;
                    polygonTemplate.tooltipText = "{name}";


                    //Set min/max fill color for each area
                    polygonSeries.heatRules.push({
                      property: "fill",
                      target: polygonSeries.mapPolygons.template,
                      min: chart.colors.getIndex(1).brighten(1),
                      max: chart.colors.getIndex(1).brighten(-.7)
                    });

                    // Make map load polygon data (state shapes and names) from GeoJSON
                    polygonSeries.useGeodata = true;

                    // Set heatmap values for each state
                    polygonSeries.data = [
                      {cnt: "Philippines",
    id: "PH",
    value: 297.0
    },
    {cnt: "Pakistan",
    id: "PK",
    value: 328.0
    },
    {cnt: "South Africa ",
    id: "ZA",
    value: 374.0
    },
    {cnt: "Kuwait",
    id: "KW",
    value: 383.0
    },
    {cnt: "Morocco",
    id: "MA",
    value: 383.0
    },
    {cnt: "Saudi Arabia",
    id: "SA",
    value: 398.0
    },
    {cnt: "Oman",
    id: "OM",
    value: 431.0
    },
    {cnt: "Chile",
    id: "CL",
    value: 441.0
    },
    {cnt: "Iran",
    id: "IR",
    value: 443.0
    },
    {cnt: "Kosovo",
    id: "XK",
    value: 444.0
    },
    {cnt: "Qatar",
    id: "QA",
    value: 449.0
    },
    {cnt: "Bosnia and Herzegovina",
    id: "BA",
    value: 452.0
    },
    {cnt: "Montenegro",
    id: "ME",
    value: 453.0
    },
    {cnt: "North Macedonia",
    id: "MK",
    value: 472.0
    },
    {cnt: "Bahrain",
    id: "BH",
    value: 480.0
    },
    {cnt: "United Arab Emirates",
    id: "AE",
    value: 481.0
    },
    {cnt: "Georgia",
    id: "GE",
    value: 482.0
    },
    {cnt: "France",
    id: "FR",
    value: 485.0
    },
    {cnt: "New Zealand",
    id: "NZ",
    value: 487.0
    },
    {cnt: "Albania",
    id: "AL",
    value: 494.0
    },
    {cnt: "Armenia",
    id: "AM",
    value: 498.0
    },
    {cnt: "Spain",
    id: "ES",
    value: 502.0
    },
    {cnt: "Serbia",
    id: "RS",
    value: 508.0
    },
    {cnt: "Croatia",
    id: "HR",
    value: 509.0
    },
    {cnt: "Malta",
    id: "MT",
    value: 509.0
    },
    {cnt: "Slovak Republic",
    id: "SK",
    value: 510.0
    },
    {cnt: "Canada",
    id: "CA",
    value: 512.0
    },
    {cnt: "Kazakhstan",
    id: "KZ",
    value: 512.0
    },
    {cnt: "Italy",
    id: "IT",
    value: 515.0
    },
    {cnt: "Azerbaijan",
    id: "AZ",
    value: 515.0
    },
    {cnt: "Bulgaria",
    id: "BG",
    value: 515.0
    },
    {cnt: "Australia",
    id: "AU",
    value: 516.0
    },
    {cnt: "Poland",
    id: "PL",
    value: 520.0
    },
    {cnt: "Germany",
    id: "DE",
    value: 521.0
    },
    {cnt: "Sweden",
    id: "SE",
    value: 521.0
    },
    {cnt: "Hungary",
    id: "HU",
    value: 523.0
    },
    {cnt: "Turkey ",
    id: "TR",
    value: 523.0
    },
    {cnt: "Denmark",
    id: "DK",
    value: 525.0
    },
    {cnt: "Portugal",
    id: "PT",
    value: 525.0
    },
    {cnt: "Belgium ",
    id: "BE",
    value: 532.0
    },
    {cnt: "Cyprus",
    id: "CY",
    value: 532.0
    },
    {cnt: "Finland",
    id: "FI",
    value: 532.0
    },
    {cnt: "Czech Republic",
    id: "CZ",
    value: 533.0
    },
    {cnt: "United States",
    id: "US",
    value: 535.0
    },
    {cnt: "Netherlands",
    id: "NL",
    value: 538.0
    },
    {cnt: "Austria",
    id: "AT",
    value: 539.0
    },
    {cnt: "Lithuania",
    id: "LT",
    value: 542.0
    },
    {cnt: "Norway ",
    id: "NO",
    value: 543.0
    },
    {cnt: "Latvia",
    id: "LV",
    value: 546.0
    },
    {cnt: "Ireland",
    id: "IE",
    value: 548.0
    },
    {cnt: "England",
    id: "GB",
    value: 556.0
    },
    {cnt: "Northern Ireland",
    id: "XI",
    value: 566.0
    },
    {cnt: "Russian Federation",
    id: "RU",
    value: 567.0
    },
    {cnt: "Japan",
    id: "JP",
    value: 593.0
    },
    {cnt: "Chinese Taipei",
    id: "TW",
    value: 599.0
    },
    {cnt: "South Korea",
    id: "KR",
    value: 600.0
    },
    {cnt: "Hong Kong SAR",
    id: "HK",
    value: 602.0
    },
    {cnt: "Singapore",
    id: "SG",
    value: 625.0
    },
                    ];

                    // Set up heat legend
                    let heatLegend = chart.createChild(am4maps.HeatLegend);
                    heatLegend.series = polygonSeries;
                    heatLegend.align = "right";
                    heatLegend.valign = "bottom";
                    heatLegend.width = am4core.percent(20);
                    heatLegend.valueAxis.renderer.labels.template.fontSize = 14;
                    heatLegend.marginRight = am4core.percent(35);
                    heatLegend.minValue = 200;
                    heatLegend.maxValue = 700;

                    // Set up custom heat map legend labels using axis ranges
                    var minRange = heatLegend.valueAxis.axisRanges.create();
                    minRange.value = heatLegend.minValue;
                    minRange.label.text = "200";
                    var maxRange = heatLegend.valueAxis.axisRanges.create();
                    maxRange.value = heatLegend.maxValue;
                    maxRange.label.text = "700";

                    // Blank out internal heat legend value axis labels
                    heatLegend.valueAxis.renderer.labels.template.adapter.add("text", function(labelText) {
                      return "";
                    });

                    // Configure series tooltip
                    var polygonTemplate = polygonSeries.mapPolygons.template;
                    polygonTemplate.tooltipText = "[bold]{cnt}[\] \n Mathematics result: {value}";
                    polygonTemplate.nonScalingStroke = true;
                    polygonTemplate.strokeWidth = 0.5;

                    // Create hover state and set alternative fill color
                    var hs = polygonTemplate.states.create("hover");
                    hs.properties.fill = am4core.color("#3c5bdc");

                    }); // end am4core.ready()
