am5.ready(function() {

  // Create root element
  // https://www.amcharts.com/docs/v5/getting-started/#Root_element
var root = am5.Root.new("chart_3a");

// Set themes
// https://www.amcharts.com/docs/v5/concepts/themes/
root.setThemes([
  am5themes_Animated.new(root)
]);

// Create chart
// https://www.amcharts.com/docs/v5/charts/xy-chart/
var chart = root.container.children.push(am5xy.XYChart.new(root, {
  panX: true,
  panY: true,
  wheelX: "panY",
  wheelY: "zoomY"
}));

chart.get("colors").set("step", 3);


// Add cursor
// https://www.amcharts.com/docs/v5/charts/xy-chart/cursor/
var cursor = chart.set("cursor", am5xy.XYCursor.new(root, {}));
cursor.lineY.set("visible", true);


// Create axes
// https://www.amcharts.com/docs/v5/charts/xy-chart/axes/
var yRenderer = am5xy.AxisRendererY.new(root, {
  minGridDistance: 15,
  inversed: true
});

var yAxis = chart.yAxes.push(am5xy.CategoryAxis.new(root, {
  maxDeviation: 0.3,
  categoryField: "category",
  renderer: yRenderer,
  tooltip: am5.Tooltip.new(root, {})
}));

var xAxis = chart.xAxes.push(am5xy.ValueAxis.new(root, {
  maxDeviation: 0.3,
  renderer: am5xy.AxisRendererX.new(root, {})
}));


// Create series
// https://www.amcharts.com/docs/v5/charts/xy-chart/series/
var series = chart.series.push(am5xy.ColumnSeries.new(root, {
  xAxis: xAxis,
  yAxis: yAxis,
  baseAxis: yAxis,
  valueXField: "close",
  openValueXField: "open",
  categoryYField: "category",
  tooltip: am5.Tooltip.new(root, {
    labelText: "Score 2015: {openValueX} \n Score 2019: {valueX}"
  })
}));

series.columns.template.setAll({
  height: 0.5
});

series.bullets.push(function() {
  return am5.Bullet.new(root, {
    locationX: 0,
    sprite: am5.Circle.new(root, {
      radius: 5,
      fill: series.get("fill")
    })
  })
})

var nextColor = chart.get("colors").getIndex(1);

// Create range axis data item
let rangeDataItem = yAxis.makeDataItem({
  category: "Saudi Arabia"
});

// Create a range
let range = yAxis.createAxisRange(rangeDataItem);

rangeDataItem.get("label").setAll({
  fill: am5.color(0xffffff),
  text: "Saudi Arabia",
  // location: 1,
  background: am5.RoundedRectangle.new(root, {
    fill: nextColor
  })
});

rangeDataItem.get("grid").set("visible", false);

series.bullets.push(function() {
  return am5.Bullet.new(root, {
    locationX: 1,
    sprite: am5.Circle.new(root, {
      radius: 5,
      fill: nextColor
    })
  })
})

var data = []
var score_2015 = [597.0,
569.0,
571.0,
556.0,
544.0,
519.0,
527.0,
512.0,
530.0,
530.0,
522.0,
537.0,
493.0,
507.0,
546.0,
499.0,
513.0,
509.0,
466.0,
457.0,
477.0,
454.0,
471.0,
455.0,
426.0,
456.0,
443.0,
411.0,
396.0,
393.0,
371.0,
398.0,
358.0,
];
var score_2019 = [608.0,
574.0,
570.0,
561.0,
543.0,
534.0,
530.0,
528.0,
523.0,
522.0,
521.0,
517.0,
515.0,
513.0,
504.0,
500.0,
499.0,
495.0,
486.0,
475.0,
473.0,
462.0,
460.0,
457.0,
452.0,
449.0,
447.0,
444.0,
431.0,
394.0,
389.0,
377.0,
370.0,
];

var diff = [
  11.0,
5.0,
-1.0,
5.0,
-1.0,
15.0,
2.0,
16.0,
-7.0,
-8.0,
-1.0,
-20.0,
22.0,
7.0,
-42.0,
2.0,
-14.0,
-13.0,
20.0,
18.0,
-4.0,
8.0,
-11.0,
3.0,
26.0,
-7.0,
4.0,
33.0,
35.0,
1.0,
19.0,
-21.0,
12.0,
]
var names = ['Singapore',
'Chinese Taipei',
'Japan',
'South Korea',
'Russia',
'Lithuania',
'Hungary',
'Australia',
'Ireland',
'USA',
'Sweden',
'England',
'Turkey',
'Israel',
'Hong Kong',
'Italy',
'New Zealand',
'Norway',
'Bahrain',
'Qatar',
'UAE',
'Chile',
'Malaysia',
'Oman',
'Jordan',
'Iran',
'Georgia',
'Kuwait',
'Saudi Arabia',
'Morocco',
'Egypt',
'Lebanon',
'South Africa',];


for (var i = 0; i < names.length; i++) {


  data.push({ category: names[i], open: score_2015[i], close: score_2019[i] });
}

yAxis.data.setAll(data);
series.data.setAll(data);


// Make stuff animate on load
// https://www.amcharts.com/docs/v5/concepts/animations/
series.appear(1000);
chart.appear(1000, 100);

}); // end am5.ready()
